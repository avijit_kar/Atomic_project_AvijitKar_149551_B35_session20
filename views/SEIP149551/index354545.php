<?php


use App\Person;
use App\Student;

function __autoload($className)
{
  //  echo $className;
    list($ns,$cn)=explode("\\",$className);
    require_once("../../src/BITM/SEIP149551/".$cn.".php");
}

//require_once("../../src/BITM/SEIP149551/Person.php");
//require_once("../../src/BITM/SEIP149551/Student.php");

$obj=new Person();
echo $obj->showPersonInfo();
$objStudent = new Student();
echo $objStudent->showStudentInfo();